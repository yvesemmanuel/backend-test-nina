import json
import boto3
from http import HTTPStatus

dynamodb = boto3.client('dynamodb')


def lambda_handler(event, context):

    post = json.loads(event['body'])

    response = dynamodb.put_item(
        TableName='string',
        Item=post
    )

    return {
        'isBase64Encoded': False,
        'statusCode': HTTPStatus.OK,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'multiValueHeaders': dict(),
        'body': json.dumps(response)
    }
